FROM node:12.16.1-alpine as builder

RUN apk update && apk upgrade && apk add --no-cache make git && rm -rf /var/cache/apk/* /tmp/*

WORKDIR /app
COPY package.json ./

COPY . .
EXPOSE 4200

